package com.artifactory.security;

import com.artifactory.model.Shouter;
import com.artifactory.repository.ShouterRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserDetailsServiceImplTest {

    private String username = "some-username";

    @Test
    public void testLoadUserByUsername() throws Exception {

        //given
        String password = "password";
        String role = "role";
        List authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(role));

        Shouter shouter = Shouter.builder()
                .username(username)
                .password(password)
                .build();

        ShouterRepository mockShouterRepository = mock(ShouterRepository.class);
        when(mockShouterRepository.findOneByUsername(eq(username))).thenReturn(shouter);
        UserDetailsServiceImpl userDetailsService = new UserDetailsServiceImpl(mockShouterRepository);

        //when
        UserDetails userDetails = userDetailsService.loadUserByUsername(username);

        //then
        assertEquals("User name should be email value supplied", username, userDetails.getUsername());
        assertEquals("Password should be value supplied", password, userDetails.getPassword());
        verify(mockShouterRepository).findOneByUsername(eq(username));
    }

    @Test(expected = UsernameNotFoundException.class)
    public void testThatLoadUserByUsernameThrowsExceptionWhenEmailNotFound() throws Exception {

        //given
        ShouterRepository mockAccountRepository = mock(ShouterRepository.class);
        when(mockAccountRepository.findOneByUsername(anyString())).thenReturn(null);
        UserDetailsServiceImpl userDetailsService = new UserDetailsServiceImpl(mockAccountRepository);

        //when
        userDetailsService.loadUserByUsername(username);
    }
}