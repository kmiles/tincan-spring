package com.artifactory.controller;

import com.artifactory.model.Shout;
import com.artifactory.model.Shouter;
import com.artifactory.service.ShoutService;
import com.artifactory.service.ShouterService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class ShoutControllerTest {

    private final String shoutText = "any-text";
    private final String shoutUserId = "some-username";
    private final String shoutId = "some-shoutId";

    @Mock
    ShoutService shoutService;

    @Mock
    ShouterService shouterService;

    @Mock
    UserDetailsService userDetailsService;

    @InjectMocks
    ShoutController shoutController;

    @Test
    public void should_create_shout() throws Exception {
        //given
        Shout shout = createValidShout("_one");

        when(shoutService.create(any(Shout.class))).thenReturn(shout);

        //when
        Shout foundShout = shoutController.create(shout);

        //then
        verify(shoutService).create(eq(shout));

        assertNotNull(foundShout);
        assertThat(foundShout.getUserId(), equalTo(shout.getUserId()));
        assertThat(foundShout.getShout(), equalTo(shout.getShout()));
    }

    @Test
    public void should_delete_shout() throws Exception {
        //given

        //when
        shoutController.delete(shoutId);

        //then
        verify(shoutService).delete(eq(shoutId));
    }

    @Test
    public void should_get_shouts() throws Exception {
        //given
        List<Shout> shouts = new ArrayList<>();
        Shout shout = createValidShout("_two");
        shouts.add(shout);

        when(shoutService.getShouts(anyString())).thenReturn(shouts);

        //when
        List<Shout> foundShouts = shoutController.get(shout.getUserId());

        //then
        verify(shoutService).getShouts(eq(shout.getUserId()));

        assertNotNull(foundShouts);
        assertThat(foundShouts.size(), equalTo(1));
        assertThat(foundShouts.get(0).getUserId(), equalTo(shout.getUserId()));
        assertThat(foundShouts.get(0).getShout(), equalTo(shout.getShout()));
    }

    @Test
    public void should_get_all_shouts_from_all_shouters() throws Exception {
        //given
        List<Shout> shouts = new ArrayList<>();
        Shout shout = createValidShout("_three");
        shouts.add(shout);

        when(shoutService.getShouts(anyString())).thenReturn(shouts);

        //when
        List<Shout> foundShouts = shoutController.get(shout.getUserId());

        //then
        verify(shoutService).getShouts(eq(shout.getUserId()));

        assertNotNull(foundShouts);
        assertThat(foundShouts.size(), equalTo(1));
        assertThat(foundShouts.get(0).getUserId(), equalTo(shout.getUserId()));
        assertThat(foundShouts.get(0).getShout(), equalTo(shout.getShout()));
    }

    @Test
    public void should_add_favorite_shout_to_shouter() throws Exception {
        //given
        Shouter shouter = Shouter.builder()
                .username(shoutUserId)
                .build();

        Shout shout = createValidShout("_three");
        shouter.getFavoriteShouts().add(shout.getId());

        when(shouterService.favoriteShout(anyString(), anyString())).thenReturn(shouter);

        //when
        Shouter foundShouter = shoutController.favorite(shouter.getUsername(), shout.getId());

        //then
        verify(shouterService).favoriteShout(eq(shouter.getUsername()), eq(shout.getId()));

        assertNotNull(foundShouter);
        assertThat(foundShouter.getUsername(), equalTo(shouter.getUsername()));
        assertThat(foundShouter.getFavoriteShouts().size(), equalTo(1));
        assertThat(foundShouter.getFavoriteShouts().get(0), equalTo(shout.getId()));
    }

    @Test
    public void should_remove_favorite_shout_from_shouter() throws Exception {
        //given
        Shouter shouter = Shouter.builder()
                .username(shoutUserId)
                .build();

        Shout shout = createValidShout("_three");

        when(shouterService.unfavoriteShout(anyString(), anyString())).thenReturn(shouter);

        //when
        Shouter foundShouter = shoutController.unfavorite(shouter.getUsername(), shout.getId());

        //then
        verify(shouterService).unfavoriteShout(eq(shouter.getUsername()), eq(shout.getId()));

        assertNotNull(foundShouter);
        assertThat(foundShouter.getUsername(), equalTo(shouter.getUsername()));
        assertThat(foundShouter.getFavoriteShouts().size(), equalTo(0));
    }

    private Shout createValidShout(String i) {

        return Shout.builder()
                .id(shoutId)
                .userId(shoutUserId + i)
                .shout(shoutText + i)
                .build();
    }
}