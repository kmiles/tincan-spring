package com.artifactory.controller;

import com.artifactory.TincanApplication;
import com.artifactory.exceptions.ShouterNotFoundException;
import com.artifactory.model.Shouter;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.response.ValidatableResponse;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.RestAssured.preemptive;
import static org.apache.http.HttpStatus.SC_CONFLICT;
import static org.apache.http.HttpStatus.SC_NOT_FOUND;
import static org.apache.http.HttpStatus.SC_OK;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(TincanApplication.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
public class ShouterControllerSmokeTest {

    @Value("${local.server.port}")
    int port;

    private final String USERNAME = "testUser";
    private final String USERNAME_1 = "testUser1";
    private final String USERNAME_2 = "testUser2";

    private String loginShouterId;

    @Before
    public void setUp() {
        RestAssured.port = port;

        Shouter loginShouter = createShouter(USERNAME, USERNAME);
        loginShouterId = loginShouter.getId();
        RestAssured.authentication = preemptive().basic(USERNAME, USERNAME);
    }

    @After
    public void tearDown() {
        deleteShouter(loginShouterId);
        RestAssured.reset();
    }

    @Test
    public void should_not_create_shouter_that_already_exists_409() throws Exception {

        Shouter user = Shouter.builder()
                .username(USERNAME)
                .password(USERNAME)
                .build();

        given().
                body(user).
                contentType(MediaType.APPLICATION_JSON_VALUE).
                when().log().all().
                post("/shouter").
                then().log().all().
                statusCode(SC_CONFLICT);
    }

    @Test
    public void createShouter_deleteShouter_validUser_validResponse_200() throws Exception {

        Shouter shouter = null;
        try {
            shouter = createShouter(USERNAME_1, USERNAME_1);
            shouter = getShouter(shouter.getUsername(), shouter.getPassword(), SC_OK);
        } finally {
            if (shouter != null) {
                deleteShouter(shouter.getId());

            }
        }
    }

    @Test
    public void followShouter_unFollowShouter_validUsers_validResponse_200() {

        Shouter shouter1 = createShouter(USERNAME_1, USERNAME_1);
        Shouter shouter2 = createShouter(USERNAME_2, USERNAME_2);

        try {
            followShouter(shouter1.getUsername(), shouter2.getUsername());

            Shouter shouter1withFollowers = getShouter(shouter1.getUsername(), shouter1.getPassword(), SC_OK);

            assertThat(shouter1withFollowers.getFollowing(), hasItem(shouter2.getUsername()));

            unFollowShouter(shouter1.getUsername(), shouter2.getUsername());

            Shouter shouter1withoutFollowers = getShouter(shouter1.getUsername(), shouter1.getPassword(), SC_OK);

            assertThat(shouter1withoutFollowers.getFollowing(), not(hasItem(shouter2.getUsername())));
        } finally {
            deleteShouter(shouter1.getId());
            deleteShouter(shouter2.getId());
        }
    }

    @Test
    public void should_return_null_user_when_getting_invalid_shouter_404() {

        given().
                when().log().all().
                get("/shouter/{username}", "invalidUsername").
                then().log().all().
                body("status", equalTo(404)).
                body("error", equalTo("Not Found")).
                body("exception", endsWith("ShouterNotFoundException")).
                statusCode(SC_NOT_FOUND);

    }

    private void followShouter(String username, String followingName) {
        given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .when()
                .post("/shouter/follow/{myname}/{followingname}", username, followingName)
                .then().log().all()
                .body("username", equalTo(username))
                .body("following[0]", equalTo(followingName))
                .statusCode(SC_OK);
    }

    private void unFollowShouter(String username, String followingName) {
        given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .when()
                .post("/shouter/unfollow/{myname}/{followingname}", username, followingName)
                .then().log().all()
                .body("username", equalTo(username))
                .body("following", not(hasItem(followingName)))
                .statusCode(SC_OK);
    }

    public Shouter createShouter(String username, String password) {

        Shouter user = Shouter.builder()
                .username(username)
                .password(password)
                .build();

        return given().
                body(user).
                contentType(MediaType.APPLICATION_JSON_VALUE).
                when().log().all().
                post("/shouter").
                then().log().all().
                body("username", equalTo(username)).
                body("password", equalTo(password)).
                statusCode(SC_OK).extract().response().as(Shouter.class);
    }

    public Shouter getShouter(String username, String password, int expectedResponse) {

        ValidatableResponse then = given().
                when().log().all().
                get("/shouter/{username}", username).
                then();

        then.log().all();

        if (SC_OK == expectedResponse) {
            return then.
                    body("username", equalTo(username)).
                    body("password", equalTo(password)).
                    extract().response().as(Shouter.class);
        }

        then.statusCode(expectedResponse);

        return null;
    }

    public void deleteShouter(String id) {

        given().
                when().
                delete("/shouter/{id}", id).
                then().
                statusCode(SC_OK);
    }
}