package com.artifactory.controller;

import com.artifactory.TincanApplication;
import com.artifactory.model.Shout;
import com.artifactory.model.Shouter;
import com.jayway.restassured.RestAssured;
import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.ArrayList;
import java.util.List;

import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.RestAssured.preemptive;
import static org.apache.http.HttpStatus.SC_OK;
import static org.hamcrest.Matchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(TincanApplication.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ShoutControllerSmokeTest {

    private final String USERNAME_1 = "testUser1";
    private final String USERNAME_2 = "testUser2";
    public static final String TEST_SHOUT = "TEST_SHOUT";

    @Value("${local.server.port}")
    int port;

    @Before
    public void setUp() {
        RestAssured.port = port;
    }

    @Test
    public void createShout_validUser_validShoutResponse() {
        createShouter(USERNAME_1, USERNAME_1);
        RestAssured.authentication = preemptive().basic(USERNAME_1, USERNAME_1);

        createShout(USERNAME_1, TEST_SHOUT);
        String id = getUserShout(USERNAME_1, TEST_SHOUT);
        deleteShout(id);
        deleteShouter(USERNAME_1);
        RestAssured.reset();
    }

    @Test
    public void getAllShouts_multipleValidUsers_validShoutResponse() {

        createShouter(USERNAME_1, USERNAME_1);
        RestAssured.authentication = preemptive().basic(USERNAME_1, USERNAME_1);
        String testShout_2 = TEST_SHOUT + "_" + USERNAME_2;

        createShout(USERNAME_1, TEST_SHOUT);
        createShout(USERNAME_2, testShout_2);
        String id1 = getUserShout(USERNAME_1, TEST_SHOUT);
        String id2 = getUserShout(USERNAME_2, testShout_2);

        List<String> userIds = new ArrayList<>();
        userIds.add(USERNAME_1);
        userIds.add(USERNAME_2);

        try {
            given().
                    contentType(MediaType.APPLICATION_JSON_VALUE).
                    body(userIds).
                    when().
                    post("/shout/all/").
                    then().log().all().
                    body("userId.collect { it.length() }.sum()", greaterThanOrEqualTo(2)).
                    body("userId", hasItems(USERNAME_1, USERNAME_2)).
                    body("shout", hasItems(TEST_SHOUT, testShout_2)).
                    statusCode(SC_OK);
        } finally {
            deleteShout(id1);
            deleteShout(id2);
            deleteShouter(USERNAME_1);
            RestAssured.reset();
        }
    }

    @Test
    public void favoriteShout_validUsers_validResponse() {

        createShouter(USERNAME_1, USERNAME_1);
        RestAssured.authentication = preemptive().basic(USERNAME_1, USERNAME_1);
        createShouter(USERNAME_2, USERNAME_2);

        String testShout_1 = TEST_SHOUT + "_" + USERNAME_1;
        String testShout_2 = TEST_SHOUT + "_" + USERNAME_2;
        String userName_1 = USERNAME_1;
        String userName_2 = USERNAME_2;

        createShout(userName_1, testShout_1);
        createShout(userName_2, testShout_2);
        String id1 = getUserShout(userName_1, testShout_1);
        String id2 = getUserShout(userName_2, testShout_2);

        try {
            favoriteShout(userName_2, id1);
        } finally {
            deleteShout(id1);
            deleteShout(id2);
            deleteShouter(USERNAME_2);
            deleteShouter(USERNAME_1);
            RestAssured.reset();
        }

    }

    @Test
    public void unfavoriteShout_validUsers_validResponse() {

        createShouter(USERNAME_1, USERNAME_1);
        RestAssured.authentication = preemptive().basic(USERNAME_1, USERNAME_1);
        createShouter(USERNAME_2, USERNAME_2);

        String testShout_1 = TEST_SHOUT + "_" + USERNAME_1;
        String testShout_2 = TEST_SHOUT + "_" + USERNAME_2;
        String userName_1 = USERNAME_1;
        String userName_2 = USERNAME_2;

        createShout(userName_1, testShout_1);
        createShout(userName_2, testShout_2);
        String id1 = getUserShout(userName_1, testShout_1);
        String id2 = getUserShout(userName_2, testShout_2);

        try {
            favoriteShout(userName_2, id1);
            unfavoriteShout(userName_2, id1);
        } finally {
            deleteShout(id1);
            deleteShout(id2);
            deleteShouter(USERNAME_2);
            deleteShouter(USERNAME_1);
            RestAssured.reset();
        }

    }

    private void deleteShout(String id) {
        given().
                when().
                delete("/shout/{id}", id).
                then().
                statusCode(SC_OK);
    }

    private String getUserShout(String username, String testShout) {
        String id =
                given().
                        when().
                        get("/shout/all/{userId}", username).
                        then().
                        body("userId.collect { it.length() }.sum()", greaterThan(0)).
                        body("[0].userId", equalTo(username)).
                        body("[0].shout", equalTo(testShout)).
                        statusCode(SC_OK).extract().path("[0].id");

        return id;
    }

    private void createShout(String username, String testShout) {

        Shout shout = Shout.builder()
                .userId(username)
                .shout(testShout)
                .build();

        given().
                contentType(MediaType.APPLICATION_JSON_VALUE).
                body(shout).
                when().
                post("/shout").
                then().
                body("userId", equalTo(username)).
                body("shout", equalTo(testShout)).
                statusCode(SC_OK);
    }

    private void favoriteShout(String userName_2, String id1) {
        given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .when()
                .post("/shout/favorite/{userId}/{shoutId}", userName_2, id1)
                .then().log().all()
                .body("username", equalTo(userName_2))
                .body("favoriteShouts[0]", equalTo(id1))
                .statusCode(SC_OK);
    }

    private void unfavoriteShout(String userName_2, String id1) {
        given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .when()
                .post("/shout/unfavorite/{userId}/{shoutId}", userName_2, id1)
                .then().log().all()
                .body("username", equalTo(userName_2))
                .body("favoriteShouts", not(hasItem(id1)))
                .statusCode(SC_OK);
    }

    public void createShouter(String username, String password) {

        Shouter user = Shouter.builder()
                .username(username)
                .password(password)
                .build();

        given().
                body(user).
                contentType(MediaType.APPLICATION_JSON_VALUE).
                when().
                post("/shouter").
                then().log().all().
                body("username", equalTo(username)).
                body("password", equalTo(password)).
                statusCode(HttpStatus.SC_OK);

    }

    public void deleteShouter(String username) {

        Object id = given().
                when().log().all().
                get("/shouter/{username}", username).
                then().log().all().
                extract().path("id");

        given().
                when().log().all().
                delete("/shouter/{id}", id).
                then().log().all().
                statusCode(HttpStatus.SC_OK);
    }
}