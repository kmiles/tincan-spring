package com.artifactory.service;

import com.artifactory.exceptions.ShouterNotFoundException;
import com.artifactory.model.Shouter;
import com.artifactory.repository.ShouterRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ShouterServiceTest {

    @Mock
    ShouterRepository shouterRepository;

    @InjectMocks
    ShouterService shouterService;

    @Test
    public void should_get_all_shouters_getAll() throws Exception {

        Shouter shouter = Shouter.builder()
                .username("anyuser")
                .password("anypassword")
                .build();

        List<Shouter> shouters = new ArrayList<>();
        shouters.add(shouter);

        //given
        when(shouterRepository.findAll()).thenReturn(shouters);

        //when
        List<Shouter> all = shouterService.getAll();

        //then
        verify(shouterRepository).findAll();

        assertThat(all.size(), equalTo(1));
        assertThat(all.get(0), equalTo(shouter));
        assertThat(all.get(0).getUsername(), equalTo(shouter.getUsername()));
    }

    @Test
    public void should_get_shouter_getShouter() throws Exception {

        Shouter shouter = Shouter.builder()
                .username("anyuser")
                .password("anypassword")
                .build();

        //given
        when(shouterRepository.findOneByUsername(anyString())).thenReturn(shouter);

        //when
        Shouter shouterFound = shouterService.getShouter(shouter.getUsername());

        //then
        verify(shouterRepository).findOneByUsername(shouter.getUsername());

        assertNotNull(shouterFound);
        assertThat(shouterFound.getUsername(), equalTo(shouter.getUsername()));
    }

    @Test(expected = ShouterNotFoundException.class)
    public void should_throw_proper_exception_when_shouter_not_found_getShouter() throws Exception {

        Shouter shouter = Shouter.builder()
                .username("anyuser")
                .password("anypassword")
                .build();

        //given
        when(shouterRepository.findOneByUsername(anyString())).thenReturn(null);

        //when
        shouterService.getShouter(shouter.getUsername());
    }

    @Test
    public void shouter_should_be_added_to_follow_list_followShouter() throws Exception {

        Shouter shouter = Shouter.builder()
                .username("anyuser")
                .password("anypassword")
                .build();

        String anyOtherUser = "anyOtherUser";

        //given
        when(shouterRepository.findOneByUsername(anyString())).thenReturn(shouter);

        //when
        Shouter shouterFound = shouterService.followShouter(shouter.getUsername(), anyOtherUser);

        //then
        verify(shouterRepository).findOneByUsername(shouter.getUsername());

        assertNotNull(shouterFound);
        assertThat(shouterFound.getUsername(), equalTo(shouter.getUsername()));
        Assert.assertThat(shouterFound.getFollowing().size(), equalTo(1));
        Assert.assertThat(shouterFound.getFollowing().get(0), equalTo(anyOtherUser));
    }

    @Test
    public void shouter_should_be_removed_from_follow_list_unfollowShouter() throws Exception {

        String anyOtherUser = "anyOtherUser";

        Shouter shouter = Shouter.builder()
                .username("anyuser")
                .password("anypassword")
                .build();
        shouter.getFollowing().add(anyOtherUser);

        //given
        when(shouterRepository.findOneByUsername(anyString())).thenReturn(shouter);

        //when
        Shouter shouterFound = shouterService.unfollowShouter(shouter.getUsername(), anyOtherUser);

        //then
        verify(shouterRepository).findOneByUsername(shouter.getUsername());

        assertNotNull(shouterFound);
        assertThat(shouterFound.getUsername(), equalTo(shouter.getUsername()));
        Assert.assertThat(shouterFound.getFollowing().size(), equalTo(0));
    }

    @Test
    public void shout_should_be_added_to_favorite_list_favoriteShout() throws Exception {

        Shouter shouter = Shouter.builder()
                .username("anyuser")
                .password("anypassword")
                .build();

        String anyShoutId = "anyOtherUser";

        //given
        when(shouterRepository.findOneByUsername(anyString())).thenReturn(shouter);

        //when
        Shouter shouterFound = shouterService.favoriteShout(shouter.getUsername(), anyShoutId);

        //then
        verify(shouterRepository).findOneByUsername(shouter.getUsername());

        assertNotNull(shouterFound);
        assertThat(shouterFound.getUsername(), equalTo(shouter.getUsername()));
        Assert.assertThat(shouterFound.getFavoriteShouts().size(), equalTo(1));
        Assert.assertThat(shouterFound.getFavoriteShouts().get(0), equalTo(anyShoutId));
    }

    @Test
    public void shout_should_be_removed_from_favorite_list_unfavoriteShout() throws Exception {

        String anyShoutId = "anyOtherUser";

        Shouter shouter = Shouter.builder()
                .username("anyuser")
                .password("anypassword")
                .build();
        shouter.getFavoriteShouts().add(anyShoutId);

        //given
        when(shouterRepository.findOneByUsername(anyString())).thenReturn(shouter);

        //when
        Shouter shouterFound = shouterService.unfavoriteShout(shouter.getUsername(), anyShoutId);

        //then
        verify(shouterRepository).findOneByUsername(shouter.getUsername());

        assertNotNull(shouterFound);
        assertThat(shouterFound.getUsername(), equalTo(shouter.getUsername()));
        Assert.assertThat(shouterFound.getFavoriteShouts().size(), equalTo(0));

    }

}