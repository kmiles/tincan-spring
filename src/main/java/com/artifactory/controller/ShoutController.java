package com.artifactory.controller;

import com.artifactory.model.Shout;
import com.artifactory.model.Shouter;
import com.artifactory.service.ShoutService;
import com.artifactory.service.ShouterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/shout")
public class ShoutController {

    private ShoutService shoutService;
    private ShouterService shouterService;

    @Autowired
    public ShoutController(ShoutService shoutService, ShouterService shouterService) {
        this.shoutService = shoutService;
        this.shouterService = shouterService;
    }

    @RequestMapping(method = RequestMethod.POST)
    public Shout create(@RequestBody Shout shout) {
        return shoutService.create(shout);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/all/{userId}")
    public List<Shout> get(@PathVariable String userId) {
        return shoutService.getShouts(userId);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/all")
    public List<Shout> get(@RequestBody List<String> userIds) {
        return shoutService.getAllShouts(userIds);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "{id}")
    public void delete(@PathVariable String id) {
        shoutService.delete(id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/favorite/{userId}/{shoutId}")
    public Shouter favorite(@PathVariable String userId, @PathVariable String shoutId) {
        return shouterService.favoriteShout(userId, shoutId);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/unfavorite/{userId}/{shoutId}")
    public Shouter unfavorite(@PathVariable String userId, @PathVariable String shoutId) {
        return shouterService.unfavoriteShout(userId, shoutId);
    }
}
