package com.artifactory.controller;

import com.artifactory.model.Shouter;
import com.artifactory.service.ShouterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/shouter")
public class ShouterController {

    private ShouterService shouterService;

    @Autowired
    public ShouterController(ShouterService shouterService) {
        this.shouterService = shouterService;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/all")
    public List<Shouter> getAll() {
        return shouterService.getAll();
    }

    @RequestMapping(method = RequestMethod.GET)
    public Principal user(Principal user) {
        return user;
    }

    @RequestMapping(method = RequestMethod.GET, value = "{username}")
    public Shouter get(@PathVariable String username) {
        return shouterService.getShouter(username);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Shouter create(@RequestBody Shouter shouter) {
        return shouterService.save(shouter);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "{id}")
    public void delete(@PathVariable String id) {
        shouterService.delete(id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/follow/{myname}/{followingname}")
    public Shouter follow(@PathVariable String myname, @PathVariable String followingname) {
        return shouterService.followShouter(myname, followingname);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/unfollow/{myname}/{followingname}")
    public Shouter unfollow(@PathVariable String myname, @PathVariable String followingname) {
        return shouterService.unfollowShouter(myname, followingname);
    }
}
