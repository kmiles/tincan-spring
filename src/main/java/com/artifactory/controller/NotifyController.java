package com.artifactory.controller;

import com.artifactory.model.ShoutMessage;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class NotifyController {


    @MessageMapping("/shouting")
    @SendTo("/topic/shoutings")
    public ShoutMessage greeting(ShoutMessage message) throws Exception {
        return message;
    }

}
