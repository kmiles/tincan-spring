package com.artifactory.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document
public class Shout {

    private static ShoutBuilder builder;

    @Id
    private String id;
    private String userId;
    private String shout;
    private Date date;

    public Shout() {
        date = new Date();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getShout() {
        return shout;
    }

    public void setShout(String shout) {
        this.shout = shout;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public static ShoutBuilder builder( ) {
        builder = new ShoutBuilder();
        return builder;
    }

    public static class ShoutBuilder {

        private Shout shout;

        private ShoutBuilder() {
            shout = new Shout();
        }

        public ShoutBuilder id(String id) {
            shout.setId(id);
            return builder;
        }

        public ShoutBuilder userId(String userId) {
            shout.setUserId(userId);
            return builder;
        }

        public ShoutBuilder shout(String shoutText) {
            shout.setShout(shoutText);
            return builder;
        }

        public ShoutBuilder date(Date date) {
            shout.setDate(date);
            return builder;
        }

        public Shout build() {
            return shout;
        }

    }

}
