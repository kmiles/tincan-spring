package com.artifactory.model;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document
public class Shouter {

    private static ShouterBuilder builder;

    @Id
    private String id;
    private String username;
    private String password;
    private List<String> following = new ArrayList<>();
    private List<String> favoriteShouts = new ArrayList<>();

    public Shouter() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<String> getFollowing() {
        return following;
    }

    public void setFollowing(List<String> following) {
        this.following = following;
    }

    public List<String> getFavoriteShouts() {
        return favoriteShouts;
    }

    public void setFavoriteShouts(List<String> favoriteShouts) {
        this.favoriteShouts = favoriteShouts;
    }

    public static ShouterBuilder builder( ) {
        builder = new ShouterBuilder();
        return builder;
    }

    public static class ShouterBuilder {

        private Shouter shouter;

        private ShouterBuilder() {
            shouter = new Shouter();
        }

        public ShouterBuilder id(String id) {
            shouter.setId(id);
            return builder;
        }

        public ShouterBuilder username(String username) {
            shouter.setUsername(username);
            return builder;
        }

        public ShouterBuilder password(String password) {
            shouter.setPassword(password);
            return builder;
        }

        public ShouterBuilder following(List<String> following) {
            shouter.setFollowing(following);
            return builder;
        }

        public ShouterBuilder favorites(List<String> favorites) {
            shouter.setFavoriteShouts(favorites);
            return builder;
        }

        public Shouter build() {
            return shouter;
        }

    }
}
