package com.artifactory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TincanApplication {

    public static void main(String[] args) {
        SpringApplication.run(TincanApplication.class, args);
    }
}
