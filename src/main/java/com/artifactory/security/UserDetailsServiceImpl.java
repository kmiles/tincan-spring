package com.artifactory.security;

import com.artifactory.model.Shouter;
import com.artifactory.repository.ShouterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import static java.lang.String.format;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private ShouterRepository users;

    @Autowired
    public UserDetailsServiceImpl(ShouterRepository users) {
        this.users = users;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Shouter shouter = users.findOneByUsername(username);

        if (shouter == null) {
            throw new UsernameNotFoundException(format("Username %s was not found.", username));
        }

        return new User(
                shouter.getUsername(),
                shouter.getPassword(),
                AuthorityUtils.createAuthorityList("USER")
        );
    }
}