package com.artifactory.service;

import com.artifactory.exceptions.InvalidUserIdShoutIdException;
import com.artifactory.exceptions.ShouterAlreadyExistsException;
import com.artifactory.exceptions.ShouterNotFoundException;
import com.artifactory.model.Shouter;
import com.artifactory.repository.ShoutRepository;
import com.artifactory.repository.ShouterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShouterService {

    private ShouterRepository shouters;
    private ShoutRepository shouts;

    @Autowired
    public ShouterService(ShouterRepository shouters, ShoutRepository shouts) {
        this.shouters = shouters;
        this.shouts = shouts;
    }


    public List<Shouter> getAll() {
        return shouters.findAll();
    }

    public Shouter getShouter(String username) {
        Shouter byUsername = shouters.findOneByUsername(username);
        if (byUsername == null) throw new ShouterNotFoundException();
        return byUsername;
    }

    public Shouter save(Shouter shouter) throws ShouterAlreadyExistsException {
        try {
            Shouter foundShouter = getShouter(shouter.getUsername());

        } catch (Exception e) {
            return shouters.save(shouter);
        }
        throw new ShouterAlreadyExistsException();
    }

    public void delete(String id) {
        shouters.delete(id);
    }

    public Shouter followShouter(String myname, String followingname) {
        Shouter shouter = shouters.findOneByUsername(myname);
        shouter.getFollowing().add(followingname);
        shouters.save(shouter);
        return shouter;
    }

    public Shouter unfollowShouter(String myname, String followingname) {
        Shouter shouter = shouters.findOneByUsername(myname);
        shouter.getFollowing().remove(followingname);
        shouters.save(shouter);
        return shouter;
    }

    public Shouter favoriteShout(String userId, String shoutId) {
        if (userId == null || shoutId == null) throw new InvalidUserIdShoutIdException();
        Shouter shouter = shouters.findOneByUsername(userId);
        shouter.getFavoriteShouts().add(shoutId);
        shouters.save(shouter);
        return shouter;

    }

    public Shouter unfavoriteShout(String userId, String shoutId) {
        if (userId == null || shoutId == null) throw new InvalidUserIdShoutIdException();
        Shouter shouter = shouters.findOneByUsername(userId);
        shouter.getFavoriteShouts().remove(shoutId);
        shouters.save(shouter);
        return shouter;
    }
}
