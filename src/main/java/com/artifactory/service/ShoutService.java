package com.artifactory.service;

import com.artifactory.model.Shout;
import com.artifactory.repository.ShoutRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ShoutService {

    private ShoutRepository shoutRepository;

    @Autowired
    public ShoutService(ShoutRepository shoutRepository) {
        this.shoutRepository = shoutRepository;
    }

    public Shout create(Shout shout) {
        Shout saved = shoutRepository.save(shout);
        return saved;
    }

    public List<Shout> getShouts(String userId) {
        List<Shout> byUserId = shoutRepository.findByUserId(userId);
        if (byUserId == null) return new ArrayList<>();
        return byUserId;
    }

    public List<Shout> getAllShouts(List<String> userIds) {
        List<Shout> byUserIds = new ArrayList<>();
        for (String userId : userIds) {
            List<Shout> byUserId = shoutRepository.findByUserId(userId);
            byUserIds.addAll(byUserId);
        }
        if (byUserIds.size() == 0) return new ArrayList<>();
        return byUserIds;
    }

    public void delete(String id) {
        shoutRepository.delete(id);
    }
}
