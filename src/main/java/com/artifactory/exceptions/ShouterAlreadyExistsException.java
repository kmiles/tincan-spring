package com.artifactory.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT, reason = "Shouter already exists.")  // 404
public class ShouterAlreadyExistsException extends RuntimeException {
}