package com.artifactory.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "userId and shoutId must be supplied")  // 404
public class InvalidUserIdShoutIdException extends RuntimeException {
}