package com.artifactory.repository;

import com.artifactory.model.Shouter;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ShouterRepository extends MongoRepository<Shouter, String> {
    public Shouter findOneByUsername(String username);
}
