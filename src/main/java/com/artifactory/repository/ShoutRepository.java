package com.artifactory.repository;

import com.artifactory.model.Shout;
import com.artifactory.model.Shouter;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface ShoutRepository extends MongoRepository<Shout, String> {
    List<Shout> findByUserId(String userId);
}
