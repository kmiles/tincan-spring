angular.module('MainCtrl', ['ngCookies'])

.controller('MainController', ['$scope', '$location', 'AuthService', 'UserService', 'ShoutService',
  function($scope, $location, AuthService, UserService, ShoutService) {

    var initLoginObject = function() {
      $scope.login.loggedin = false;
      $scope.login.loginFailed = false;
      $scope.login.username = '';
      $scope.login.password = '';
    }

    var initCreateObject = function() {
      $scope.create.passwordsMatch = true;
      $scope.username = '';
      $scope.create.password = '';
      $scope.create.passwordConfirm = '';
      $scope.create.placeholder = 'enter password'
    }

    var successfulLogin = function(user) {
      if (user.username === undefined) return;
      //login success
      $scope.activeUser = user;
      $scope.login.loggedin = true;
      $scope.login.password = '';
      $scope.create = {};
    }

    var failedLogin = function() {
      $scope.create.password = '';
      $scope.create.passwordConfirm = '';
      $scope.login.password = '';
      $scope.login.loginFailed = true;
    }

    var passwordsDontMatch = function() {
      $scope.create.username = '';
      $scope.create.passwordsMatch = false;
      $scope.create.password = '';
      $scope.create.passwordConfirm = '';
      $scope.create.placeholder = 'passwords must match'
    }

    var userExists = function( username ) {
      $scope.login.username = username;
      $scope.create.passwordsMatch = false;
      $scope.create.password = '';
      $scope.create.passwordConfirm = '';
      $scope.create.placeholder = 'user already exists'
    }

    var loadUser = function( username ) {
        UserService.getUser(username)
        .then(function(response) {
            successfulLogin(response.data);
            $location.path('/userhome');
          },
          function(error) {
            alert('Failure!')
          });
    }

    var createUser = function() {

      UserService.createUser({
          username: $scope.create.username,
          password: $scope.create.password
        })
        .then(function(response) {
            $scope.login.username = $scope.create.username;
            $scope.login.password = $scope.create.password;
            $scope.signIn();
          },
          function(error) {
            alert('Failure!')
          });
    }

    $scope.login = {
      username: '',
      password: '',
      loggedin: false,
      loginFailed: false
    }

    $scope.create = {
      passwordsMatch: true,
      username: '',
      password: '',
      passwordConfirm: '',
      placeholder: 'enter password'
    }

    initLoginObject();
    initCreateObject();

    $scope.tagline = 'Tincan - Shout out your thoughts!';

    $scope.signIn = function() {

      AuthService.authenticate({
          username: $scope.login.username,
          password: $scope.login.password
        })
        .then(function(response) {
            if (response.data === "") {
              failedLogin();
            } else {
              loadUser( $scope.login.username);
            }
          },
          function(error) {
            alert('Failure!')
          });

    }

    $scope.createNewUser = function() {

      if ($scope.create.username === '' || $scope.create.password === '') {
        return;
      }

      if ($scope.create.password !== $scope.create.passwordConfirm) {
        passwordsDontMatch();
        return;
      }

      createUser();

    }

    $scope.logout = function() {
      AuthService.logout().then(function(response) {
        ShoutService.subscribe( null );
          $scope.activeUser = {};
          initCreateObject();
          initLoginObject();
          $location.path('/');
      },
      function(error) {
        alert("Could Not Logout!")
      });

    }

    $scope.goToUser = function() {
      $location.path('/userhome');
    }

  }
]);
