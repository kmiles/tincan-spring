angular.module('UserCtrl', [])

.filter('isaFavoriteShout', function() {
  return function(items, active, favioriteShouts) {
    if (!active) return items;
    var filtered = [];
    for (var i = 0; i < items.length; i++) {
      var item = items[i];
      if (favioriteShouts.indexOf(item.id) > -1) {
        filtered.push(item);
      }
    }
    return filtered;
  };
})

.controller('UserController', ['$scope', 'UserService', 'ShoutService',
  function($scope, UserService, ShoutService) {

    var getAllFollowing = function () {
        following = [];
        if ($scope.activeUser.following.length > 0) {
            following = $scope.activeUser.following.slice(0);
        }
        following.push($scope.activeUser.username);
        return following;
    }

    ShoutService.subscribe(function(userId) {
        if ($scope.isFollowingCaller(userId)) {
            $scope.refreshAllShouts();
        }
    });

    $scope.tagline = UserService.getTagline();
    $scope.hasShouts = false;

    $scope.viewOnlyFavoitedShouts = false;
    $scope.shout = {
      text: ''
    }

    $scope.shouts = [];

    UserService.get().then(function(response) {
      $scope.users = response.data;
    });

    $scope.createShout = function() {
      ShoutService.createShout({
          userId: $scope.activeUser.username,
          shout: $scope.shout.text
        })
        .then(function(response) {
            $scope.shout.text = '';
            $scope.refreshAllShouts();
          },
          function(error) {
            alert('Failure!')
          });
    }

    $scope.getShouts = function(username) {
      ShoutService.getShouts({
          userId: username
        })
        .then(function(response) {
            return response.data;
          },
          function(error) {
            alert('Failure!')
          });
    }

    $scope.getAllShouts = function(userIds) {
      ShoutService.getAllShouts({
          userIds: userIds
        })
        .then(function(response) {
            $scope.shouts = response.data;
            $scope.hasShouts = $scope.shouts.length > 0;
          },
          function(error) {
            alert('Failure!')
          });
    }

    $scope.refreshAllShouts = function() {
      var following = getAllFollowing();
      $scope.getAllShouts(following);
    }

    $scope.toggleFollowCaller = function(username, myname) {

      if ($scope.isFollowingCaller(username)) {

        UserService.unFollowCaller(username, myname)
          .then(function(response) {
              $scope.activeUser.following = response.data.following;
            },
            function(error) {
              alert('Failure!')
            });

      } else {

        UserService.followCaller(username, myname)
          .then(function(response) {
              $scope.activeUser.following = response.data.following;
            },
            function(error) {
              alert('Failure!')
            });

      }
    }

    $scope.isFollowingCaller = function(username) {
        if( $scope.activeUser.username === undefined ) return;
        var following = false;

        if ($scope.activeUser.following.length > 0) {
          following = $scope.activeUser.following.indexOf(username) > -1;
        }
        return following;
    }

    $scope.isFavoriteShout = function(shoutId) {
        if( $scope.activeUser.username === undefined ) return;
        var favorite = false;
        if ($scope.activeUser.favoriteShouts.length > 0) {
          favorite =  $scope.activeUser.favoriteShouts.indexOf(shoutId) > -1;
        }
        return favorite;
    }

    $scope.toggleFavoriteShout = function(userId, shout) {
      if ($scope.isFavoriteShout(shout.id)) {
        ShoutService.unFavoriteShout(userId, shout.id)
          .then(function(response) {
              $scope.activeUser.favoriteShouts = response.data.favoriteShouts;
            },
            function(error) {
              alert('Failure!')
            });
      } else {
        ShoutService.favoriteShout(userId, shout.id)
          .then(function(response) {
              $scope.activeUser.favoriteShouts = response.data.favoriteShouts;
            },
            function(error) {
              alert('Failure!')
            });
      }
    }

    $scope.toggleViewFavoriteShouts = function() {
      $scope.viewOnlyFavoitedShouts = !$scope.viewOnlyFavoitedShouts;
    }

    $scope.refreshAllShouts();
  }
]);
