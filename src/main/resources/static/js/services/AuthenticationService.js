
angular.module('AuthService', [])

.factory('AuthService', ['$http', '$q', function($http, $q) {

    return {
        authenticate : function(login) {

            var url = '/shouter/';

            var headers = login ? {authorization : "Basic "
                + btoa(login.username + ":" + login.password)
            } : {};

            var promise = $http.get(url, {headers : headers}).
            success(function(response) {
              return response;
            }).
            error(function(data) {
              console.log('Failed to login' + data);
            });

            return promise;
        },

        logout : function() {

             var url = 'logout';

             var promise = $http.post(url, {}).
              success(function(response) {
                return response;
              }).
              error(function(data) {
                console.log('Failed to logout' + data);
              });

              return promise;
         }
    }

}]);
