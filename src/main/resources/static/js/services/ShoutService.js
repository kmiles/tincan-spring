angular.module('ShoutService', [])

.factory('ShoutService', ['$http', function($http) {

    var stompClient = null;
    var subscriptionCallback = null;

    var connect = function( callback ) {

        subscriptionCallback = callback;

        if( stompClient === null ) {

            var socket = new SockJS('/shouting');
            stompClient = Stomp.over(socket);

            stompClient.connect('','', function(frame) {
                stompClient.subscribe('/topic/shoutings', function(shoutMessage){
                    if( subscriptionCallback === null ) return;
                    subscriptionCallback(JSON.parse(shoutMessage.body).userId);
                });
            });

        }
    };

    var disconnect = function() {
        if (stompClient != null) {
            stompClient.disconnect();
            stompClient = null;
        }
    };

    var notify = function( userId ) {
        stompClient.send("/notify/shouting", {}, JSON.stringify({ 'userId': userId }));
    };

  return {

    subscribe: function( callback ) {

        if( callback === null ) {
            disconnect();
            return;
        }

        connect(callback);
    },

    createShout: function(shout) {

      var url = '/shout';

      var promise = $http.post(url, shout).
      success(function(response) {
        notify( shout.userId )
        return response.data;
      }).
      error(function(data) {
        return data;
      });

      return promise;
    },

    getShouts: function(user) {

      var url = '/shout/all/' + user.userId;

      var promise = $http.get(url).
      success(function(response, status, headers, config) {
        return response.data;
      }).
      error(function(data, status, headers, config) {
        return data;
      });

      return promise;
    },

    getAllShouts: function(userIds) {

      var url = '/shout/all';

      var promise = $http.post(url, userIds.userIds).
      success(function(response) {
        return response;
      }).
      error(function(data, status, headers, config) {
        return data;
      });

      return promise;
    },

    favoriteShout: function(userId, shoutId) {

      var url = '/shout/favorite/' + userId + '/' + shoutId;

      var promise = $http.post(url).
      success(function(response) {
        return response;
      }).
      error(function(data) {
        return data;
      });

      return promise;
    },

    unFavoriteShout: function(userId, shoutId) {

      var url = '/shout/unfavorite/' + userId + '/' + shoutId;

      var promise = $http.post(url).
      success(function(response) {
        return response;
      }).
      error(function(data) {
        return data;
      });

      return promise;
    }
    
  }

}]);
