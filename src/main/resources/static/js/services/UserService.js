angular.module('UserService', [])

.factory('UserService', ['$http', function($http) {

  return {
    get: function() {

      var url = '/shouter/all';

      var promise = $http.get(url).
      success(function(response) {
        return response.data;
      }).
      error(function(data) {
        return data;
      });

      return promise;
    },

    getUser: function(username) {

      var url = '/shouter/' + username;

      var promise = $http.get(url).
      success(function(response) {
        return response;
      }).
      error(function(data) {
        return data;
      });

      return promise;
    },

    createUser: function(payload) {

      var url = '/shouter';

      var promise = $http.post(url, payload).
      success(function(response) {
        return response;
      }).
      error(function(data) {
        return data
      });

      return promise;
    },

    followCaller: function(username, myname) {

      var url = '/shouter/follow/' + myname + '/' + username;

      var promise = $http.post(url).
      success(function(response, status, headers, config) {
        return response;
      }).
      error(function(data, status, headers, config) {
        return data;
      });

      return promise;
    },

    unFollowCaller: function(username, myname) {

      var url = '/shouter/unfollow/' + myname + '/' + username;

      var promise = $http.post(url).
      success(function(response) {
        return response;
      }).
      error(function(data) {
        return data;
      });

      return promise;
    },

    getTagline: function() {
      return 'List of Users';
    }
  }

}]);
