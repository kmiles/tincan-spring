// public/js/appRoutes.js
angular.module('appRoutes', [])

    .config(['$routeProvider', '$locationProvider', '$httpProvider',
        function($routeProvider, $locationProvider, $httpProvider) {

        $routeProvider

            // home page
            .when('/', {
                templateUrl: 'views/home.html'
            })

            .when('/userhome', {
                templateUrl: 'views/user.html',
                controller: 'UserController'
            })

            .when('/userlist', {
                templateUrl: 'views/userlist.html',
                controller: 'UserController'
            });

        $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';

        $locationProvider.html5Mode(true);

    }]);
