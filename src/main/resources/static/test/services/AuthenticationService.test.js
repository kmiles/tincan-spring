describe('AuthService', function () {

  var AuthService;
  var $httpBackend;

  beforeEach(module('AuthService'));

  beforeEach(inject(function (_AuthService_, _$httpBackend_) {
    AuthService = _AuthService_;
    $httpBackend = _$httpBackend_;
  }));

  describe('authenticate', function () {

    it('makes a post call with the object', function () {
      var login = {username:'foo', password:'bar'};
      var succeeded;

      $httpBackend
      .expectPOST('/api/authenticate', login)
      .respond({success: '200'});

      AuthService.authenticate(login)
      .then(function () {
        succeeded = true;
      });
      $httpBackend.flush();
      expect(succeeded).to.be.true;
    });

  });

});
