describe('Main Controller', function() {
  var scope, location, cookies, AuthService, UserService, $q, deferred, $controller;

  beforeEach(module('MainCtrl',['ngCookies']));

  beforeEach(module(function($provide) {

    AuthService = {
      authenticate: sinon.stub()
    };

    $provide.value('AuthService', AuthService);

    UserService = {
      getUser: sinon.stub(),
      createUser: sinon.stub()
    };

    $provide.value('UserService', UserService);

    cookies = {
      getObject: sinon.stub(),
      putObject: sinon.stub()
    };

    $provide.value('cookies', cookies);

    location = {
      path: sinon.stub()
    };

    $provide.value('$location', location);

  }));


  beforeEach(inject(function(_$controller_, $rootScope, _$q_) {
    scope = $rootScope.$new();
    $q = _$q_;
    $controller = _$controller_;
  }));

  compile = function() {
    controller = $controller('MainController', {
      $scope: scope
    });
  }

  describe('Page load', function() {
    beforeEach(compile);

    it('should have tagline', function() {
      expect(scope.tagline).to.equal('Tincan - Shout out your thoughts!');
    });
  });

  describe('Sign in', function() {
    beforeEach(function() {
      deferred = $q.defer();
      AuthService.authenticate = sinon.stub().returns(deferred.promise);
      compile();
    });

    it('should set location path on login success', function() {
      scope.login.username = 'foo';
      scope.login.password = 'bar';
      scope.signIn();

      deferred.resolve({
        data: {
          success: true
        }
      });

      scope.$apply();

      expect(location.path).to.be.calledWith('/user');
    });
  });
});
