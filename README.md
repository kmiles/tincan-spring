# README #

### Tincan ###

* Gaslight Twitter Clone
* 0.0.1

### How do I get set up? ###

* Summary of set up
    * npm install
        * This executes: bower install
    * ./gradlew clean build bootRun
* Configuration
* Dependencies
    * Java 1.8
    * Springboot
        * gradle
    * npm
    * MongoDB
* Database configuration
    * MongoDB
        * env var: MONGODB_URI=<your mongodb>
* How to run tests
    * ./gradlew clean build
* Deployment instructions
    * ./gradlew bootRun

### MVP requirements ###
* signup
* posting new status updates
* following other users 'callers'
* seeing your timeline
* favoriting shouts

### Working functionality ###
* create new user
* login
* logout
* create shouts
* follow other users 'callers'

### Refactor Tasks ###
* make login/signup ui more intuitive

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
    * kevin j miles : mileskj@gmail.com
* Other community or team contact